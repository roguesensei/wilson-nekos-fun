# Wilson Extension: Nekos.Fun
Adds commands that utilize a few of the endpoints from the [Nekos.Fun](https://www.nekos.fun/) API.
